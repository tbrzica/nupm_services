﻿using Microsoft.AspNetCore.Mvc;

namespace Ispadi_Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "Done";
        }
    }
}
